# -*- coding: utf-8 -*-

""" Importing libraries and external files """

import unittest
import time
from selenium import webdriver
import HtmlTestRunner

# """ Testing login activity on http://dec.vumk.eu/student/web/site/login """

class LoginTest(unittest.TestCase):
    """ test """
    def setUp(self):
        self.driver = webdriver.Chrome("C:/D/ChromeDriver/chromedriver.exe")
        self.driver.implicitly_wait(15)
        self.verification_errors = []  # pylint: disable=invalid-name
        self.accept_next_alert = True

    def test_login(self):
        """ test """
        driver = self.driver
        driver.get("http://dec.vumk.eu/student/web/site/login")
        time.sleep(5)
        driver.find_element_by_id("UserLoginForm_username").clear()
        driver.find_element_by_id("UserLoginForm_username").send_keys("frenafrani@gmail.com")
        driver.find_element_by_id("UserLoginForm_password").clear()
        driver.find_element_by_id("UserLoginForm_password").send_keys("123456")
        driver.find_element_by_id("login-content-button").click()
        print("Expected web page")
        time.sleep(10)
        driver.get_screenshot_as_file('login_test.png')
        time.sleep(2)
        self.assertIn(driver.current_url, "http://dec.vumk.eu/student/web/")

    def test_wrong_login(self):
        """ test """
        driver = self.driver
        driver.get("http://dec.vumk.eu/student/web/site/login")
        time.sleep(5)
        driver.find_element_by_id("UserLoginForm_username").clear()
        driver.find_element_by_id("UserLoginForm_username").send_keys("frenafrani@gmail.com")
        driver.find_element_by_id("UserLoginForm_password").clear()
        driver.find_element_by_id("UserLoginForm_password").send_keys("654321")
        driver.find_element_by_id("login-content-button").click()
        print("Expected password error message")
        time.sleep(10)
        driver.get_screenshot_as_file('wrong_login_test.png')
        time.sleep(2)
        self.assertIn(driver.current_url, "http://dec.vumk.eu/student/web/site/login")

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verification_errors)

if __name__ == "__main__":
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output='example_dir'))
